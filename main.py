import time

from telegram import Bot
from telegram.ext import Updater
from sqlitedict import SqliteDict

import atm_loader
import handlers.start
import handlers.help
import handlers.stop
import handlers.settings
import handlers.location
import map
from utils.config import apikey

db = SqliteDict('db.sqlite', autocommit=True)

print("Starting ...")
updater = Updater(token=apikey, use_context=True)
dispatcher = updater.dispatcher
dispatcher.add_handler(handlers.start.create())
dispatcher.add_handler(handlers.help.create())
dispatcher.add_handler(handlers.stop.create())

handlers.settings.registration(dispatcher)
handlers.location.registration(dispatcher)

updater.start_polling()

bot = Bot(token=apikey)

while True:
    updates = {}
    images = {}
    try:
        for (chat_id, settings) in db.items():
            atms = atm_loader.GetAvialableATM(settings["location"]["lat"],
                                              settings["location"]["lng"],
                                              int(settings["zoom"]),
                                              settings["currency"],
                                              int(settings["amount"]))
            for atm in atms:
                if atm.id not in images:
                    images[atm.id] = map.get_image(atm.lat, atm.lng)

            for atm in atms:
                limits = [item for item in atm.limits if item["currency"] == settings["currency"] \
                          and int(item["amount"]) >= int(settings["amount"])]

                if atm.id in settings["atms_ids"] or len(limits) == 0:
                    continue
                settings["atms_ids"].append(atm.id)
                updates[chat_id] = settings
                bot.send_photo(chat_id=chat_id, caption=f"{atm.address},\nВ наличии:<b>{limits[0]['amount']}</b>",
                               parse_mode="HTML", photo=images[atm.id])
        db.update(updates)
    except BaseException:
        pass

    time.sleep(60)
