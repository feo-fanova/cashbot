import requests
from datetime import date
from types import SimpleNamespace
import geopy.distance


def GetAvialableATM(lat, lng, z, currency, amount):
    zoom = {16: 1, 15: 2, 14: 4, 13: 8, 12: 16, 11: 32}
    km = zoom[z]
    distance = geopy.distance.distance(kilometers=km)
    bottomLeft = distance.destination((lat, lng), bearing=225)
    topRight = distance.destination((lat, lng), bearing=45)

    url = "https://api.tinkoff.ru/geo/withdraw/clusters"
    data = {
        "bounds": {
            "bottomLeft": {"lat": bottomLeft.latitude, "lng": bottomLeft.longitude},
            "topRight": {"lat": topRight.latitude, "lng": topRight.longitude}},
        "filters": {"showUnavailable": True, "currencies": [currency], "amounts": [{"currency": currency, "amount": amount}]},
        "zoom": z
    }

    response = requests.post(url=url, json=data).json()
    today = date.today().strftime("%d-%m-%Y")

    result = []
    for c in response["payload"]["clusters"]:
        for p in c["points"]:
            r = SimpleNamespace()
            r.id = p["id"]
            r.address = p["address"]
            r.updated = today
            r.lat = p["location"]["lat"]
            r.lng = p["location"]["lng"]
            r.limits = p["limits"]

            result.append(r)

    return result
