from sqlitedict import SqliteDict
from telegram.ext import CommandHandler
from telegram.ext import CallbackContext
from telegram import Update

db = SqliteDict('db.sqlite', autocommit=True)

def start(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Ожидайте, мы вам сообщим, когда банкоматы будут пополнены. \n"
                                  "Для настройки поиска перейдите в /settings")
    db[update.effective_chat.id] = {
        "currency": "USD",
        "amount": 100,
        "atms_ids": [],
        "location": {
            "lat": 55.74529142636622,
            "lng": 37.63643669060061
        },
        "zoom": 12
    }

def create():
    start_handler = CommandHandler('start', start)
    return start_handler