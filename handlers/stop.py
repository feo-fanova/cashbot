from sqlitedict import SqliteDict
from telegram.ext import CommandHandler
from telegram.ext import CallbackContext
from telegram import Update

db = SqliteDict('db.sqlite', autocommit=True)

def stop(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Спасибо, что воспользовались нашим ботом")
    db.pop(update.effective_chat.id)

def create():
    return CommandHandler('stop', stop)