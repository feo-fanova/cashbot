from sqlitedict import SqliteDict
from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram.ext import CallbackContext
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup

db = SqliteDict('db.sqlite', autocommit=True)


def currency_markup():
    list_of_currency = ['EUR', 'USD']
    button_list = []
    for each in list_of_currency:
        button_list.append(InlineKeyboardButton(each, callback_data="/Currency " + each))
    reply_markup = InlineKeyboardMarkup([button_list])
    return reply_markup


def amount_markup():
    amount = ['100', '1000', '3000', '5000']
    button_list = []
    for each in amount:
        button_list.append(InlineKeyboardButton(each, callback_data="/Amount " + each))
    reply_markup = InlineKeyboardMarkup([button_list[:2], button_list[2:]])
    return reply_markup


def settings(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text='Выберите валюту', reply_markup=currency_markup())


def currency_clicked(update: Update, context: CallbackContext):
    data = update.callback_query.data
    currency = data.split(sep=" ")[1]
    chat_id = update.effective_chat.id

    user_settings = db[chat_id]
    user_settings["currency"] = currency
    db.update({chat_id: user_settings})
    context.bot.send_message(chat_id=chat_id, text="Выберите минимальную сумму в банкомате",
                             reply_markup=amount_markup())


def amount_clicked(update: Update, context: CallbackContext):
    data = update.callback_query.data
    amount = data.split(sep=" ")[1]
    chat_id = update.effective_chat.id

    user_settings = db[chat_id]
    user_settings["amount"] = amount
    db.update({chat_id: user_settings})
    context.bot.send_message(chat_id=chat_id, text="Спасибо, ожидайте.Для выбора геолокации используйте /location")


def registration(dispatcher):
    setting_handler = CommandHandler('settings', settings)
    dispatcher.add_handler(setting_handler)

    currency_handler = CallbackQueryHandler(callback=currency_clicked, pattern="/Currency*")
    dispatcher.add_handler(currency_handler)

    amount_handler = CallbackQueryHandler(callback=amount_clicked, pattern="/Amount*")
    dispatcher.add_handler(amount_handler)
