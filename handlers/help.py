from telegram.ext import CommandHandler
from telegram.ext import CallbackContext
from telegram import Update

def help(update: Update, context: CallbackContext):
    message = "/start - запустить\n" \
              "/stop - остановить\n" \
              "/settings - выбрать валюту\n" \
              "/location - выбрать геолокацию\n" \
              "/help - справка"
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)

def create():
    start_handler = CommandHandler('help', help)
    return start_handler