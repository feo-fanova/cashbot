from sqlitedict import SqliteDict
from telegram.ext import CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram.ext import CallbackContext
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton, ReplyKeyboardMarkup

db = SqliteDict('db.sqlite', autocommit=True)
def location_markup():
    button = KeyboardButton("Передать местоположение", request_location=True)
    return ReplyKeyboardMarkup([[button]])


def location(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text='Разрешите запрос геолокации', reply_markup=location_markup())


def zoom_markup():
    zoom = {"1 км": 16, "2 км": 15, "4 км": 14, "8 км": 13, "16 км": 12, "32 км": 11}
    button_list = []
    for (text, zoom) in zoom.items():
        button_list.append(InlineKeyboardButton(text, callback_data="/Zoom " + str(zoom)))
    reply_markup = InlineKeyboardMarkup([button_list[:2], button_list[2:4], button_list[4:]])
    return reply_markup


def location_clicked(update: Update, context: CallbackContext):
    data = update.message.effective_attachment
    chat_id = update.effective_chat.id
    user_settings = db[chat_id]
    user_settings["location"]["lat"] = data["latitude"]
    user_settings["location"]["lng"] = data["longitude"]
    db.update({chat_id: user_settings})

    context.bot.send_message(chat_id=chat_id, text="Укажите радиус поиска",
                             reply_markup=zoom_markup())

def zoom_clicked(update: Update, context: CallbackContext):
    data = update.callback_query.data
    zoom = data.split(sep=" ")[1]
    chat_id = update.effective_chat.id

    user_settings = db[chat_id]
    user_settings["zoom"] = zoom
    db.update({chat_id: user_settings})
    context.bot.send_message(chat_id=chat_id, text="Спасибо, ожидайте. Список команд /help")



def registration(dispatcher):
    location_handler = CommandHandler('location', location)
    dispatcher.add_handler(location_handler)

    dispatcher.add_handler(MessageHandler(filters=Filters.location, callback=location_clicked))

    amount_handler = CallbackQueryHandler(callback=zoom_clicked, pattern="/Zoom*")
    dispatcher.add_handler(amount_handler)
